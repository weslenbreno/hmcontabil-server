const mongoose = require('mongoose');
var db = null;

mongoose.Promise = global.Promise;

exports.connect = (url='mongodb://localhost/hmc-server') => {
  
  mongoose.connect(url, (err, mongodb) => {
    if(err) throw err;
    db = mongodb;
  });
}

exports.close = () => { 
  if(db) return db.close() 

  return null;
};

exports.db = () => db;