const paymentService = require('../services/payment');

exports.create = (req, res) => {
  return paymentService.create(req.body, res);
}

exports.delete = (req, res) => {
  return paymentService.delete(req.params.id, res);
}

exports.update = (req, res) => {
  return paymentService.update(req.body, res);
}

exports.getCompanyPayments = (req, res) => {
  return paymentService.getCompanyPayments(req.params.company, res);
}

exports.getMonthPayments = (req, res) => {
  return paymentService.getMonthPayments(req.query.month, res);
}

exports.getAll = (req, res) => {

  if(req.query.month) {
    return paymentService.getMonthPayments(req.query.month, res);
  }
  
  if(req.query.id){
    return companySrv.getPayment(req.query.id, res);
  }
  return paymentService.getAllPayments(res);
}

exports.getPaid = (req, res) => {
  return paymentService.getPaid(res);
}

exports.getPendentes = (req, res) => {
  return paymentService.getPendentes(res);
}
