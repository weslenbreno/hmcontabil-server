const userSrv = require('../services/user');

exports.create = (req, res) => {
  return userSrv.create(req.body, res);
}

exports.delete = (req, res) => {
  return userSrv.delete(req.params.id, res);
}

exports.update = (req, res) => {
  return userSrv.update(req.body, res);
}

exports.getAll = (req, res) => {
  if(req.query.id){
    return companySrv.getUser(req.query.id, res);
  }
  return userSrv.getAll(res);
}