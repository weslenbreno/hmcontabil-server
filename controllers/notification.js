const noteService = require('../services/notification');

exports.create = (req, res) => {
  return noteService.create(req.body, res);
}

exports.delete = (req, res) => {
  return noteService.delete(req.params.id, res);
}

exports.update = (req, res) => {
  return noteService.update(req.body, res);
}

exports.getAll = (req, res) => {
  if(req.query.id){
    return noteService.getNote(req.query.id, res);
  }
  return noteService.getAllNotes(res);
}
