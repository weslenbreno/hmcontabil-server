const todoService = require('../services/todo');

exports.create = (req, res) => {
  return todoService.create(req.body, res);
}

exports.delete = (req, res) => {
  return todoService.delete(req.params.id, res);
}

exports.update = (req, res) => {
  return todoService.update(req.body, res);
}

exports.getAll = (req, res) => {
  if(req.query.id){
    return companySrv.getTodo(req.query.id, res);
  }
  return todoService.getAllTodos(res);
}