const companySrv = require('../services/campany');

exports.create = (req, res) => {
  return companySrv.create(req.body, res);
}

exports.delete = (req, res) => {
  return companySrv.delete(req.params.id, res);
}

exports.update = (req, res) => {
  return companySrv.update(req.body, res);
}

exports.getAll = (req, res) => {
  if(req.query.id){
    return companySrv.getCompany(req.query.id, res);
  }
  return companySrv.getAll(res);
}

exports.updateScript = (req, res) => {
  return companySrv.updateScript(res);
}