const agendaService = require('../services/agenda');

exports.create = (req, res) => {
  return agendaService.create(req.body, res);
}

exports.delete = (req, res) => {
  return agendaService.delete(req.params.id, res);
}

exports.update = (req, res) => {
  return agendaService.update(req.body, res);
}

exports.getAll = (req, res) => {

  if(req.query.id) {
    return  agendaService.agendaService.getAgenda(req.query.id, res);
  }
  return agendaService.getAllAgendas(res);
}