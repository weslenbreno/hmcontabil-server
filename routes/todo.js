const express = require('express');
const router  = express.Router();

const todoController = require('../controllers/todo');

router.get('/', todoController.getAll);
router.post('/', todoController.create);
router.patch('/', todoController.update);
router.delete('/:id', todoController.delete);

module.exports = router;
