const express = require('express');
const router  = express.Router();

const notificationController = require('../controllers/notification');

router.get('/', notificationController.getAll);
router.post('/', notificationController.create);
router.patch('/', notificationController.update);
router.delete('/:id', notificationController.delete);

module.exports = router;
