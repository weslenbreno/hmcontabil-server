const express = require('express');
const router  = express.Router();

const companyController = require('../controllers/company');

router.get('/', companyController.getAll);
router.post('/', companyController.create);
router.patch('/', companyController.update);
router.delete('/:id', companyController.delete);

router.get('/update', companyController.updateScript);

module.exports = router;
