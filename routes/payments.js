const express = require('express');
const router  = express.Router();

const paymentController = require('../controllers/payment');

router.get('/', paymentController.getAll);
router.get('/', paymentController.getMonthPayments);

router.get('/pendentes', paymentController.getPendentes);
router.get('/pagos', paymentController.getPaid);

router.get('/:company', paymentController.getAll);

router.post('/', paymentController.create);
router.patch('/', paymentController.update);
router.delete('/:id', paymentController.delete);

module.exports = router;
