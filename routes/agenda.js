const express = require('express');
const router  = express.Router();

const agendaController = require('../controllers/agenda');

router.get('/', agendaController.getAll);
router.post('/', agendaController.create);
router.patch('/', agendaController.update);
router.delete('/:id', agendaController.delete);

module.exports = router;
