const express = require('express');
const router  = express.Router();

const userCrontroller = require('../controllers/user');

router.get('/', userCrontroller.getAll);
router.post('/', userCrontroller.create);
router.patch('/', userCrontroller.update);
router.delete('/:id', userCrontroller.delete);

module.exports = router;
