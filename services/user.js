const User = require('../models/User');

exports.create = (user, res) => {
  newUser = new User(user);
  newUser.save((err, item) => {
    if(err) return res.send(err);
    return res.send(item);
  })
}

exports.getUser = (id, res) => {
  Company.findOne({ '_id': id }, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.update = (user, res) => {
  User.findOne({
    '_id': user.id
  }, (err, item) => {
    if(err) return res.send(err);

    if(!item) return res.send({err: "Not found"});

    item.email = user.title || item.email;

    item.save((err, doc) => {
      if(err) return res.send(err);
      res.send(doc);
    })
  });

};

exports.getAll = (res) => {
  User.find({}, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.delete = (id, res) => {
  User.findByIdAndRemove(id, (err, item) => {
    if(err) return res.send(err);
    res.send(item);
  })
}