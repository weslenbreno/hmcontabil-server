const Query = require('mongoose').Query;

const Payment = require('../models/Payment');
const companySrv = require('./campany');

exports.create = (payment, res) => {
  newPayment = new Payment(payment);
  newPayment.save((err, item) => {
    if(err) { 
      console.log(err);
      return res.send(err);
    }
    return res.send(item);
  })
}

exports.getPayment = (id, res) => {
  Company.findOne({ '_id': id }, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.update = (payment, res) => {
  console.log(payment);
  Payment.findOne({
    $or:[ {'_id': payment.id }, {'_id': payment._id }]}, (err, item) => {
    if(err) return res.send(err);

    if(!item) return res.send({err: "Not found"});

    item.company = payment.company || item.company;
    item.body = payment.body || item.body;
    item.due_date = payment.due_date || item.due_date;
    item.pay_date = payment.pay_date;
    item.amount = payment.amount || item.amount;
    item.status = payment.status || item.status;
    item.repeat = payment.repeat || item.repeat;

    item.save((err, doc) => {
      if(err) return res.send(err);
      res.send(doc);
    })
  });
  
};

exports.getAllPayments = (res) => {
  Payment.find({}, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.delete = (id, res) => {
  Payment.findByIdAndRemove(id, (err, item) => {
    if(err) return res.send(err);

    res.send(item);
  })
}

exports.getCompanyPayments = (company, res) => {
  return companySrv.getPayments(company, res);
}

exports.getMonthPayments = (month, res) => {
  console.log(month);
} 

exports.getPendentes = (res) => {
  Payment.find({ $or:[ {'status': 'pendente' }, {'status': 'Pendente' }]},(err, docs) => {
    if(err) return res.send(err);
    let items = docs.filter(data => {
      data.due_date <= new Date().getMonth();
    })

    items = items.concat(...docs.map(pay => {
      n_mes = new Date().getMonth();
      atraso = [];
      pay.repeat.months.forEach((element, i) => {
        if(i <= n_mes && element === ''){
          let day = new Date(pay.due_date).getDate();
          if(i == 1 && day > 26) {
            day = 26;
          }

          if(i === n_mes){
            atraso.push(Object.assign(pay, {company: pay.company, due_date: new Date( 2018, i, day), status: 'Pendente'}));
          } else {
            atraso.push(Object.assign(pay, {company: pay.company, due_date: new Date( 2018, i, day), status: 'Atrasado'}));
          }
        }
      });
      return atraso;
    }))

    return res.send(items);
  }) 
}

exports.getPaid = (res) => {
  Payment.find({ $or:[ {'status': 'pago' }, {'status': 'Pago' }]}, (err, docs) => {
    if(err) return res.send(err);
    return res.send(docs);
  }) 
}
