const Todo = require('../models/Todo');

exports.create = (todo, res) => {
  newTodo = new Todo(todo);
  newTodo.save((err, item) => {
    if(err) return res.send(err);
    return res.send(item);
  })
}

exports.getTodo = (id, res) => {
  Company.findOne({ '_id': id }, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.update = (todo, res) => {
  Todo.findOne({
    '_id': todo.id
  }, (err, item) => {
    if(err) return res.send(err);

    if(!item) return res.send({err: "Not found"});

    item.title = todo.title || item.title;
    item.body = todo.body || item.body;

    item.save((err, doc) => {
      if(err) return res.send(err);
      res.send(doc);
    })
  });

};

exports.getAllTodos = (res) => {
  Todo.find({}, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.delete = (id, res) => {
  Todo.findByIdAndRemove(id, (err, item) => {
    if(err) return res.send(err);

    res.send(item);
  })
}