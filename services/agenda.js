const Agenda = require('../models/Agenda');

exports.create = (agenda, res) => {
  newAgenda = new Agenda(agenda);
  newAgenda.save((err, item) => {
    if(err) return res.send(err);
    return res.send(item);
  })
}

exports.getAgenda = (id, res) => {
  Company.findOne({ '_id': id }, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.update = (agenda, res) => {
  Agenda.findOne({
    '_id': agenda.id
  }, (err, item) => {
    if(err) return res.send(err);

    if(!item) return res.send({err: "Not found"});

    item.local = agenda.local || item.local;
    item.date = agenda.date || item.date;
    item.hour = agenda.hour || item.hour;
    
    item.save((err, doc) => {
      if(err) return res.send(err);
      res.send(doc);
    })
  });

};

exports.getAllAgendas = (res) => {
  Agenda.find({}, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.delete = (id, res) => {
  Agenda.findByIdAndRemove(id, (err, item) => {
    if(err) return res.send(err);
    res.send(item);
  })
}