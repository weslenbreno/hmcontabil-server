const Note = require('../models/Notification');

exports.create = (note, res) => {
  newNote = new Note(note);
  newNote.save((err, item) => {
    if(err) return res.send(err);
    return res.send(item);
  })
}

exports.update = (note, res) => {
  Note.findOne({
    '_id': note.id
  }, (err, item) => {
    if(err) return res.send(err);

    if(!item) return res.send({err: "Not found"});

    item.title = note.title || item.title;
    item.body = note.body || item.body;

    item.save((err, doc) => {
      if(err) return res.send(err);
      res.send(doc);
    })
  });

};

exports.getNote = (id, res) => {
  Note.findOne({ '_id': id }, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.getAllNotes = (res) => {
  Note.find({}, (err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.delete = (id, res) => {
  Note.findByIdAndRemove(id, (err, item) => {
    if(err) return res.send(err);

    res.send(item);
  })
}
