const Company = require('../models/Company');
const Payment = require('../models/Payment');

exports.create = (company, res) => {
  newCompany = new Company({ ...company, payment: company.payment.id });
  newCompany.save((err, item) => {
    if(err) { 
      console.log(err);
      return res.send(err);
    }
    console.log(item);
    return res.send(item);
  })
}

exports.update = (company, res) => {
  Company.findOne({
    '_id': company._id
  }, (err, item) => {
    if(err) return res.send(err);
    if(!item) return res.send({err: "Not found"});
        item.name = company.name || item.name;
        item.email = company.email || item.email;
        item.opening = company.opening || item.opening;
        item.phone = company.sec_phone || item.sec_phone;
        item.cnpj = company.cnpj || item.cnpj;
        item.cpf = company.cpf || item.cpf;
        item.ie = company.ie || item.ie;
        item.sn = company.sn || item.sn;
        item.cnae = company.cnae || item.cnae;
        item.owner = company.owner || item.owner;
        item.address = company.address || item.address;
        item.pass = company.pass || item.pass;
        item.cnae_sec = company.cnae_sec || item.cnae_sec;

    item.save((err, doc) => {
      if(err) return res.send(err);
      res.send(doc);
    })
  });
};

exports.getAll = (res) => {
  Company.find({})
    .populate('payment')
    .exec((err, data) => {
    if(err) return res.send(err);
    return res.send(data);
  })
}

exports.getCompany = (id, res) => {
  Company.findOne({ '_id': id })
    .populate('payment')
    .exec((err, data) => {
      if(err) return res.send(err);
      return res.send(data);
  })
}

exports.delete = (id, res) => {
  Company.findByIdAndRemove(id, (err, item) => {
    if(err) return res.send(err);
    res.send(item);
  })
}

exports.getPayments = (company, res) => {
  Payment.find({'company': company }, (err, docs) => {
    if(err) return res.send({error: 'db fail'});
    return res.send(docs);
  })
}

exports.updateScript = (res) => {
  Company.find({}, (err, item) => {

    if(err) return res.send(err);
    let arr = [];

    item.forEach(comp => {
      newPay = new Payment({
        company: comp.name,
        amount: 100,
        due_date: new Date(),
        body: 'Pagamento Mensal',
        repeat: {
          pay_type: 'monthly',
          times: 0,
          months: new Array(12).fill('')
        }
      });
      comp.payment = newPay._id;
      newPay.save();
      comp.save();
      arr.push(newPay);

    });

    return res.send(arr);
  });
}
