var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const companySchema = new Schema({
  name: String,
  owner: {
    name: String,
    birthdate: String,
  },
  opening: String,
  email: String,
  pass: String,
  phone: String,
  sec_phone: String,
  cnpj: String,
  cpf: String,
  ie: String, // Inscrição Estadual
  sn: String, // Simples Nacional
  cnae: String,  
  cnae_sec: String,  
  address: {
    city: String,
    state: String,
    cep: Number,
    location: String, 
  },
  payment: { type: Schema.Types.ObjectId, ref: 'Payment' }
});

const Company = mongoose.model('Company', companySchema );

module.exports = Company;
