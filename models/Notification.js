var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const noteSchema = new Schema({
  title: String,
  date: Date,
  hour: String,
  body: String,
  label: String,
  priority: { type: Number , default: 0 }
});

const note = mongoose.model('Note', noteSchema );

module.exports = note;