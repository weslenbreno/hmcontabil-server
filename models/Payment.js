var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const paymentSchema = new Schema({
  company: String,
  repeat: { 
    pay_type: { type: String, default: 'unique' }, 
    times: Number, 
    months: [ String ]
  },
  type: { type: String , default: 'in' },
  amount: Number,
  due_date: String,
  pay_date: String,
  body: String,
  status: { type: String , default: 'pendente' }
});

const Payment = mongoose.model('Payment', paymentSchema );
module.exports = Payment;
