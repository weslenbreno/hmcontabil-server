var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const AgendaSchema = new Schema({
  local: String,
  date: String,
  hour: String,
  label: String,
  done: { type: Boolean , default: false },
  logo: { type: String, default: 'agendamento.jpg'}
});

const Agenda = mongoose.model('Agenda', AgendaSchema );

module.exports = Agenda;
