var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const todoSchema = new Schema({
  title: String,
  date: String,
  hour: String,
  body: String,
  label: String,
  done: { type: Boolean , default: false }
});

const Todo = mongoose.model('Todo', todoSchema );

module.exports = Todo;
